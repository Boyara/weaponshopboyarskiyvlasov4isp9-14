Полноценное ТЗ находится в корне репозитория

Данная Информационная Система предназначенна для автоматизации бизнес-процессов происходящих в оружейном магазине. В этом файле представленно описание данного проекта и текущая затребованная информация.

АКТУАЛЬНЫЕ ДАННЫЕ:

ERD-Диаграмма
![Image](https://gitlab.com/Boyara/weaponshopboyarskiyvlasov4isp9-14/-/raw/main/Res/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-09-15_183026.jpg)
Диаграмма из БД:
![Image](https://gitlab.com/Boyara/weaponshopboyarskiyvlasov4isp9-14/-/raw/main/Res/ERD.png)

Use-Case Диаграмма
![Ссылка на диаграмму](https://gitlab.com/Boyara/weaponshopboyarskiyvlasov4isp9-14/-/raw/main/Res/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0_2023-09-21_183157.jpg)

Диаграмма классов
![Ссылка на диаграмму](https://gitlab.com/Boyara/weaponshopboyarskiyvlasov4isp9-14/-/raw/main/Res/UML_-_%D0%BA%D0%BB%D0%B0%D1%81%D1%81%D1%8B.png)

Диаграмма последовательностей
![Image](https://gitlab.com/Boyara/weaponshopboyarskiyvlasov4isp9-14/-/raw/main/Res/UML_-_%D0%BF%D0%BE%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D0%BD%D0%BE%D1%81%D1%82%D0%B8.png)]

Дизайн: https://www.figma.com/file/pM6mO5zslu0x1Ezo04zS97/Untitled?type=design&t=9lV3tTMHUaXIHkZY-6

Сущность Worker представляет собой работника, она имеет поля:
- id - Идентификатор
- IdAccount - Внешний ключ на сущность Account
- firstname - Имя работника
- lastname - фамилия работника
- PassportSerial - серия пасспотра
- PassportNum - номер пасспотра
- DateOfBirth - дата рождения
- Phone - телефон

Сущность Account представляет собой аккаунт в Информационной Системе, необходимый для взаимодействия в онной. Сущность имееет поля:
- id - Идентификатор
- username - Логин
- password - Пароль
- DatеOfCreation - Дата создания аккаунта
- IsAdmin - булевое значение, имеются ли у аккаунта административные полномочия

Сущность Client представляет собой клиента в Информационной системе. Заводить запись клиенту необходимо в случае приобретения товаров, для которых необходима лицензия.
(Нулевая позиция в этой сущности зарезервированна для анонимных покупателей)
Поля сущности:



