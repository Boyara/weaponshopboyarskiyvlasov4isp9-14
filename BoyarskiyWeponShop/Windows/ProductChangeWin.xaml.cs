﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BoyarskiyWeponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductChangeWin.xaml
    /// </summary>
    public partial class ProductChangeWin : Window
    {
        public ProductChangeWin()
        {
            InitializeComponent();
            ProductWin pg = new ProductWin();
            txtProdName.Text = pg.dgProduct.SelectedItem as string;
        }


    }
}
