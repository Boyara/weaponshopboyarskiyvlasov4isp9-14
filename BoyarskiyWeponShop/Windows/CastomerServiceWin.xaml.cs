﻿using BoyarskiyWeponShop.ClassHelper;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace BoyarskiyWeponShop.Windows

{
    /// <summary>
    /// Логика взаимодействия для CastomerServiceWin.xaml
    /// </summary>
    public partial class CastomerServiceWin : Window
    {
        public CastomerServiceWin()
        {
            InitializeComponent();
            lwProductListWiew.ItemsSource = EFHelper.Context.Product.ToList();
            dgProduct.ItemsSource = EFHelper.Context.Product.ToList();
        }



        private void dgProduct_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void btnAddProductToBucet_Click(object sender, RoutedEventArgs e)
        {
            foreach (DB.Product item in dgProduct.SelectedItems)
            {
                ProductBusket.busket.Add(EFHelper.Context.Product.Where(i => i.id == item.id).FirstOrDefault());
            }
            lwProductListWiew.ItemsSource = ProductBusket.busket.ToList();
        }

        private void btnBuy_Click(object sender, RoutedEventArgs e)
        {
            DB.Chek chek = new DB.Chek();
            chek.idWorker = UserData.currentAccount.id;
            chek.dateOfTransaction = DateTime.Now;
            chek.idClient = 1;
            EFHelper.Context.Chek.Add(chek);
            DB.ChekProduct chekProduct = new DB.ChekProduct();
            int currentChekID = EFHelper.Context.Chek.Max(i => i.id);
            foreach (var i in ProductBusket.busket)
            {
                chekProduct.Product = i;
                chekProduct.Chek = chek;
                EFHelper.Context.ChekProduct.Add(chekProduct);
                EFHelper.Context.SaveChanges();
                chekProduct = new DB.ChekProduct();
            }

            EFHelper.Context.SaveChanges();
            MessageBox.Show("Транзакция успешна");
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            ProductBusket.busket.Remove((DB.Product)lwProductListWiew.SelectedItem);
            lwProductListWiew.ItemsSource = ProductBusket.busket.ToList();
        }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
    }
}
