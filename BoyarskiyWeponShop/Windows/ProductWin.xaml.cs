﻿using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace BoyarskiyWeponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductWin.xaml
    /// </summary>
    public partial class ProductWin : Window
    {
        public ProductWin()
        {
            InitializeComponent();
            dgProduct.ItemsSource = ClassHelper.EFHelper.Context.Product.ToList();
        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {
            ProductChangeWin productChangeWin = new ProductChangeWin();
            productChangeWin.Show();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            ClassHelper.EFHelper.Context.SaveChanges();
        }

        private void txbSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            dgProduct.ItemsSource = ClassHelper.EFHelper.Context.Product.Where(i => i.Title.Contains(txbSearch.Text)).ToList();
        }
    }
}
