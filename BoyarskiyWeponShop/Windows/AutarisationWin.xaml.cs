﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BoyarskiyWeponShop.ClassHelper;
using BoyarskiyWeponShop.DB;

namespace BoyarskiyWeponShop.Windows
{
    /// <summary>
    /// Логика взаимодействия для AutarisationWin.xaml
    /// </summary>
    public partial class AutarisationWin : Window
    {
        public AutarisationWin()
        {
            InitializeComponent();
        }

        private void btnEnter_Click(object sender, RoutedEventArgs e)
        {
            var loginData = EFHelper.Context.Account.Where(i => i.username == txtLogin.Text && i.password == txtPassword.Text).FirstOrDefault();
            if (loginData != null)
            {

                MainWindow mainWindow = new MainWindow();
                ClassHelper.UserData.currentAccount = loginData;
                mainWindow.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Неверныек Логин или пароль");
            }
        }
    }
}
